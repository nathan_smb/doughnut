# Doughnut

Makes end-to-end testing with mocha and meteor easy.

### Installation

If you are only using the command line utility it will probably be easier to install the package globally.

> npm i -g doughnut

### What it does

This package is very simple but helpful when it comes to end-to-end testing. The process is as follows:

1. Create a new mongodb instance. (Default mongo port: 45786)
2. Create a new meteor instance set to use the new mongodb. (Default meteor port: 45787)
3. Start mocha.(Default test folder with CLI: cwd/tests)
4. If a mocha test fails exit with an error. Otherwise exit without.
5. Drop the mongodb collections and shutdown the mongodb instance.
6. Shutdown Meteor.

So really it's a little bit of automation that sets you up to write end-to-end tests the way most people are used too. Just take your favorite assertion library and your favorite headless browser and go to town.

### CLI usage

Most developers will probably only need the CLI tool. After installing you can just run the "doughnut" or "donut" command. They both direct to the same script. I included both to avoid confusion with the multiple spellings of doughnut.

###### Options:
    --mongoport         Set a different port to be used for mongodb.
    --meteorport        Set a different port to be used for meteor.
    --data              Set a custom mongodb data directory.
    --meteortimeout     Set the timeout for the meteor start up process.
    -h, --help          Show help
    -t, --mochatimeout  Sets the timeout for each mocha test.
    -r, --reporter      Set the mocha reporter.
    -u, --ui            Set the mocha ui.
    -d, --test          Set a custom directory for your test files.
    -p, --project       Set a custom directory for your meteor project
    -v, --verbose       Sets the logging to include more then just the reporter.

### Using doughnut programmatically

Using doughnut programmatically may be useful if you need additional automation. It is set up rather simply for you to use.

    const doughnut = require("doughnut");
    doughnut([doughnut options][, mocha options][, callback])

The doughnut and mocha options are both objects. All of the parameters are optional. There are sensible defaults if you choose to use none. If you decide to not include a callback then doughnut will return a promise for you.

###### Doughnut options:
    {
        mongoPort: Number (Default: 45786),
        meteorPort: Number (Default: 45787),
        dataDir: String (Default: <doughnuts directory>/data)
        testDir: String (Default: <your app directory>/tests),
        projectDir: String (Default: <your app directory>),
        verbose: opts.verbose (Default: false),
        timeout: Number (Default: 60000) // This is in milliseconds
    }

The timeout is for how long doughnut gives mongo and meteor to start up. It has nothing to do with mochas timeout.

###### Mocha options:

[Using mocha programmatically.](https://github.com/mochajs/mocha/wiki/Using-mocha-programmatically)

I only pass the mocha options directly to the mocha object. So any setting covered in the above article should apply to doughnut.

### About us.

Don’t Panic solves complicated business issues through collaborative and innovative coaching techniques, augmented with custom technology solutions.

If you have a use for any of our services you can contact us at [info@dontpanic.consulting](mailto:info@dontpanic.consulting)
