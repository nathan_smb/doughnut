module.exports = (opts, mochaOpts, cb) => {
    const
        exec = require("child_process").exec,
        setTimeout = require("timers").setTimeout,
        fs = require("fs"),
        path = require("path"),
        Mocha = require("mocha"),
        mocha = new Mocha(mochaOpts),
        defaultAppDir = __dirname.substring(0, __dirname.indexOf("/node_modules")),
        // User setable options.
        options = {
            mongoPort: opts.mongoPort || 45786,
            meteorPort: opts.meteorPort || 45787,
            dataDir: opts.dataDir ? path.resolve(__dirname, opts.dataDir) : path.join(__dirname, "data"),
            testDir: opts.testDir ? path.resolve(defaultAppDir, opts.testDir) : path.join(defaultAppDir, "tests"),
            projectDir: opts.projectDir ? path.resolve(defaultAppDir, opts.projectDir) : defaultAppDir,
            verbose: opts.verbose,
            timeout: opts.timeout || 60000
        },
        walkSync = (dir, filelist = []) => {
            fs.readdirSync(dir).forEach((file) => {
                filelist = fs.statSync(path.join(dir, file)).isDirectory()
                ? walkSync(path.join(dir, file), filelist)
                : filelist.concat(path.join(dir, file));
            });
            return filelist;
        };

    let main = (callback) => {
        // Create mongo data directory.
        if(!fs.existsSync(options.dataDir)){
            fs.mkdirSync(options.dataDir);
            if(!fs.existsSync(options.dataDir)){
                if(options.verbose){
                    console.error("Could not create " + options.dataDir);
                }
                process.exit(9);
            }
        }

        let meteor,
            started = false;

        const
            mongodb = exec("mongod --dbpath=\"" + options.dataDir + "\" --port " + options.mongoPort),
            close = (code) => {
                exec("mongo --port " + options.mongoPort + " --eval \"db.dropDatabase()\"", () => {
                    mongodb.kill();
                    // Unbind port if mongo isn't killed correctly. Happens occassionally.
                    exec("mongo --port " + options.mongoPort + " --eval \"db.getSiblingDB('admin').shutdownServer()\"", ()=>{
                        // Trying to kill meteor is wierd. Can't use meteor.kill().
                        // Need to manually kill (pid + 1).
                        if(meteor && meteor.pid){
                            exec("kill " + (meteor.pid + 1), ()=> {
                                callback(code);
                            });
                        } else {
                            callback(code);
                        }
                    });
                });
            },
            setMeteorListeners = () => {
                meteor.stdout.on("data", (data) => {
                    if(options.verbose){
                        console.log(data);
                    }
                    if(data.indexOf("App running at") !== -1){
                        started = true;
                        console.log("Meteor has started.");
                        walkSync(options.testDir).filter((file) => {
                            return file.substr(-3) === ".js";
                        }).forEach((file) => {
                            mocha.addFile(file);
                        });
                        mocha.run((err) => {
                            close(err);
                        });
                    }
                });

                meteor.stderr.on("data", (data) => {
                    if(options.verbose){
                        console.error(data);
                    }
                    if(data.indexOf("Exited with code") !== -1 || data.indexOf("You're not in a Meteor project directory.") !== -1){
                        close(6);
                    }
                });
            };

        mongodb.stdout.on("data", (data) => {
            if(options.verbose){
                console.log(data);
            }
            if(data.indexOf("waiting for connections on port") !== -1){
                console.log("MongoDB has started.");
                meteor = exec("cd \"" + options.projectDir + "\" && MONGO_URL=mongodb://localhost:" + options.mongoPort + " meteor --port " + options.meteorPort);
                setMeteorListeners();
            }
        });

        process.on("SIGINT", ()=>{
            close(2);
        });

        if(options.timeout !== 0 || !Number.isNaN(options.mongoTimeout)){
            setTimeout(()=>{
                if(!started){
                    close(14);
                }
            }, options.timeout);
        }
    };

    if(opts && typeof opts === "function"){
        cb = opts;
    } else if(mochaOpts && typeof mochaOpts === "function"){
        cb = mochaOpts;
    }

    if(cb){
        main(cb);
    } else {
        return new Promise((resolve, reject)=>{
            main((code)=>{
                if(code){
                    reject(code);
                } else {
                    resolve(1);
                }
            });
        });
    }
};
