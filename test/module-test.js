const
    expect = require("chai").expect,
    exec = require("child_process").exec,
    path = require("path"),
    rootPath = __dirname.substring(0, __dirname.indexOf("/test")),
    appPath = path.join(rootPath, "mockapp"),
    doughnut = require(path.join(rootPath, "index.js")),
    doughnutOptions = {
        mongoPort: 43840,
        meteorPort: 43841,
        dataDir: path.join(appPath, "data"),
        testDir: path.join(appPath, "tests/module"),
        projectDir: appPath
    },
    mochaOptions = {
        ui: "bdd",
        reporter: "spec",
        timeout: 100000
    };

describe("Doughnut Module", function(){
    this.timeout(0);
    it("success", (done)=>{
        exec("cd \"" + appPath + "\" && meteor npm install", () => {
            doughnut(doughnutOptions, mochaOptions).then((result) => {
                expect(result).to.equal(1);
                doughnutOptions.timeout = 1;
            }).then(()=>{
                doughnut(doughnutOptions, mochaOptions, (code) => {
                    expect(code).to.equal(14);
                    done();
                });
            });
        });
    });
});
