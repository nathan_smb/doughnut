const
    chai = require("chai"),
    Nightmare = require("nightmare");

let nightmare = Nightmare({
    show: false,
    typeInterval: 20
});

chai.should();

describe("Meteor", function() {
    this.timeout(0);
    it("Is open.", function(done){
        nightmare.goto("http://localhost:43841")
            .title()
            .end()
            .then(function(result){
                result.should.equal("mockapp");
                done();
            }).catch(function(){
                done();
            });
    });
});
