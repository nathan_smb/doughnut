const
    chai = require("chai"),
    exec = require("child_process").exec,
    path = require("path"),
    rootPath = __dirname.substring(0, __dirname.indexOf("/test")),
    appPath = path.join(rootPath, "mockapp"),
    doughnutOptions = {
        mongoPort: 39529,
        meteorPort: 39530,
        dataDir: path.join(appPath, "datacli"),
        testDir: path.join(appPath, "tests/cli"),
        projectDir: appPath
    },
    mochaOptions = {
        timeout: 100000
    },
    expect = chai.expect;

let command = () =>{
    let str = "";
    str += "cd \"" + rootPath + "\" && ";
    str += "node doughnut -t " + mochaOptions.timeout + " ";
    str += "--mongoport " + doughnutOptions.mongoPort + " ";
    str += "--meteorport " + doughnutOptions.meteorPort + " ";
    str += "--data \"" + doughnutOptions.dataDir + "\" ";
    str += "--test \"" + doughnutOptions.testDir + "\" ";
    str += "--project \"" + doughnutOptions.projectDir +"\"";
    return str;
};


describe("Doughnut CLI", function(){
    this.timeout(0);
    it("success", (done)=>{
        exec("cd \"" + appPath + "\" && meteor npm install", () => {
            exec(command(), (code)=>{
                expect(code).to.equal(null);
                doughnutOptions.projectDir = __dirname;
                exec(command(), (code)=>{
                    expect(code.code).to.equal(6);
                    done();
                });
            });
        });
    });
});
