module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "node": true,
        "meteor": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "sourceType": "module"
    },
    "rules": {
        "indent": [
            "warn",
            4
        ],
        "linebreak-style": [
            "warn",
            "unix"
        ],
        "quotes": [
            "warn",
            "double"
        ],
        "semi": [
            "warn",
            "always"
        ],
        "comma-dangle": [
            "error",
            "never"
        ],
        "no-console": "off",
        "no-global-assign": [
            "error",
            {
                "exceptions": [
                    "describe",
                    "it",
                    "should",
                    "expect"
                ]
            }
        ]
    },
    "globals": {
        "describe": false,
        "it": false,
        "should": false,
        "expect": false
    }
};
